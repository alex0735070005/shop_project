
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
 
module.exports = { 
   
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    historyApiFallback: true,
    compress: true,
    port: 9000
  }, 

  entry: './index.js',
  mode: 'none',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'build')
  },

  plugins: [new HtmlWebpackPlugin({
    title: 'Learn shop',
    filename: 'public/index.html'
  })]
};