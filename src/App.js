var Cart            = require('./Cart');
var ProductContent  = require('./ProductContent');

class App {

    constructor(products, container_id)
    {
        this.cart = new Cart(products);

        this.products  = products;
        this.container = document.getElementById(container_id);
        this.showProducts();
    }

    showProducts()
    {        
        for(var j in this.products)
        {
            var data = this.products[j];

            var product = new ProductContent(data, this.cart.add);

            this.container.appendChild(product.render());
        }
    }
}

module.exports = App;