class Product 
{
    constructor(dataProduct, callback)
    {
        this.data = dataProduct;
        this.container = document.createElement('div');

        this.name = document.createElement('h4');
        this.image = document.createElement('img');
        this.price = document.createElement('p');
        this.button = document.createElement('button');

        this.name.className  = 'p-name';
        this.image.className = 'p-img img-fluid';
        this.price.className = 'p-price';
        this.button.className = 'btn btn-primary';
        this.button.type      = 'button';

        this.name.innerHTML      = this.data.name;
        this.image.src           = this.data.image;
        this.price.innerHTML     =  this.data.price;
        this.button.setAttribute('data-product-id', this.data.id);

        if(callback){
            this.button.onclick = callback;
        }
    }
}

module.exports = Product;